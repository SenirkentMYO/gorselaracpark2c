﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace OtoparkTakip
{
    public partial class tarifeler : Form
    {
        public tarifeler()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Baglanti"].ConnectionString);           

                conn.Open();
                SqlCommand kmt = new SqlCommand();
                kmt.CommandText = "insert into tarifeler  (TarifeAdi,Sure,Fiyat) values ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "')";
                kmt.Connection = conn;
                kmt.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Tarife Oluşturuldu !");
                dataGridView1.Refresh();
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Hata" + ex);

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" & textBox2.Text != "" & textBox3.Text != "")
            {
                try
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Baglanti"].ConnectionString);           

                    conn.Open();
                    SqlCommand kmt = new SqlCommand();
                    kmt.CommandText = "delete from tarifeler  where TarifeAdi ='" + textBox1.Text + "'";
                    kmt.Connection = conn;
                    kmt.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Kayıt Silindi!");
                    dataGridView1.Refresh();
                    
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";


                }
                catch (Exception ex)
                {
                    MessageBox.Show("Hata" + ex);

                }

            }
            else
                MessageBox.Show("Boş alanları doldurunuz");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tarifeler_Load(object sender, EventArgs e)
        {
            

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
        }

    }
}

