﻿namespace OtoparkTakip
{
    partial class aracbilgileri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tur = new System.Windows.Forms.Label();
            this.yktTipi = new System.Windows.Forms.Label();
            this.arcNo = new System.Windows.Forms.TextBox();
            this.plkNo = new System.Windows.Forms.TextBox();
            this.turu = new System.Windows.Forms.TextBox();
            this.mrk = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.grs = new System.Windows.Forms.Button();
            this.cks = new System.Windows.Forms.Button();
            this.blg = new System.Windows.Forms.Label();
            this.peron = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bosYer = new System.Windows.Forms.Label();
            this.iptl = new System.Windows.Forms.Button();
            this.yktTpi = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(34, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "*Araç No:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(34, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "*Plaka No:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(34, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Marka:";
            // 
            // tur
            // 
            this.tur.AutoSize = true;
            this.tur.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tur.Location = new System.Drawing.Point(34, 160);
            this.tur.Name = "tur";
            this.tur.Size = new System.Drawing.Size(34, 17);
            this.tur.TabIndex = 3;
            this.tur.Text = "Tür:";
            // 
            // yktTipi
            // 
            this.yktTipi.AutoSize = true;
            this.yktTipi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.yktTipi.Location = new System.Drawing.Point(34, 192);
            this.yktTipi.Name = "yktTipi";
            this.yktTipi.Size = new System.Drawing.Size(75, 17);
            this.yktTipi.TabIndex = 4;
            this.yktTipi.Text = "*Yakıt Tipi:";
            // 
            // arcNo
            // 
            this.arcNo.Location = new System.Drawing.Point(123, 57);
            this.arcNo.Name = "arcNo";
            this.arcNo.Size = new System.Drawing.Size(100, 20);
            this.arcNo.TabIndex = 5;
            // 
            // plkNo
            // 
            this.plkNo.Location = new System.Drawing.Point(123, 89);
            this.plkNo.Name = "plkNo";
            this.plkNo.Size = new System.Drawing.Size(100, 20);
            this.plkNo.TabIndex = 6;
            // 
            // turu
            // 
            this.turu.Location = new System.Drawing.Point(123, 153);
            this.turu.Name = "turu";
            this.turu.Size = new System.Drawing.Size(100, 20);
            this.turu.TabIndex = 8;
            // 
            // mrk
            // 
            this.mrk.Location = new System.Drawing.Point(123, 121);
            this.mrk.Name = "mrk";
            this.mrk.Size = new System.Drawing.Size(100, 20);
            this.mrk.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(239, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 31);
            this.label4.TabIndex = 11;
            this.label4.Text = "Araç Bilgileri";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(333, 57);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(305, 169);
            this.dataGridView1.TabIndex = 12;
            // 
            // grs
            // 
            this.grs.Location = new System.Drawing.Point(245, 73);
            this.grs.Name = "grs";
            this.grs.Size = new System.Drawing.Size(75, 23);
            this.grs.TabIndex = 13;
            this.grs.Text = "Giriş  +";
            this.grs.UseVisualStyleBackColor = true;
            this.grs.Click += new System.EventHandler(this.button1_Click);
            // 
            // cks
            // 
            this.cks.Location = new System.Drawing.Point(245, 112);
            this.cks.Name = "cks";
            this.cks.Size = new System.Drawing.Size(75, 23);
            this.cks.TabIndex = 14;
            this.cks.Text = "Çıkış   -";
            this.cks.UseVisualStyleBackColor = true;
            this.cks.Click += new System.EventHandler(this.cks_Click);
            // 
            // blg
            // 
            this.blg.AutoSize = true;
            this.blg.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.blg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.blg.Location = new System.Drawing.Point(39, 224);
            this.blg.Name = "blg";
            this.blg.Size = new System.Drawing.Size(51, 17);
            this.blg.TabIndex = 15;
            this.blg.Text = "*Peron";
            // 
            // peron
            // 
            this.peron.FormattingEnabled = true;
            this.peron.Location = new System.Drawing.Point(123, 224);
            this.peron.Name = "peron";
            this.peron.Size = new System.Drawing.Size(121, 21);
            this.peron.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(34, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 17);
            this.label5.TabIndex = 17;
            this.label5.Text = "Kullanılabilir Alan";
            // 
            // bosYer
            // 
            this.bosYer.AutoSize = true;
            this.bosYer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bosYer.Location = new System.Drawing.Point(155, 250);
            this.bosYer.Name = "bosYer";
            this.bosYer.Size = new System.Drawing.Size(21, 29);
            this.bosYer.TabIndex = 18;
            this.bosYer.Text = "-";
            // 
            // iptl
            // 
            this.iptl.Location = new System.Drawing.Point(245, 147);
            this.iptl.Name = "iptl";
            this.iptl.Size = new System.Drawing.Size(75, 23);
            this.iptl.TabIndex = 19;
            this.iptl.Text = "İptal ";
            this.iptl.UseVisualStyleBackColor = true;
            this.iptl.Click += new System.EventHandler(this.iptl_Click);
            // 
            // yktTpi
            // 
            this.yktTpi.FormattingEnabled = true;
            this.yktTpi.Location = new System.Drawing.Point(123, 188);
            this.yktTpi.Name = "yktTpi";
            this.yktTpi.Size = new System.Drawing.Size(121, 21);
            this.yktTpi.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(369, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(202, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "*Yıldızlı Alanların Doldurulması Zorunludur!";
            // 
            // aracbilgileri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 294);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.yktTpi);
            this.Controls.Add(this.iptl);
            this.Controls.Add(this.bosYer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.peron);
            this.Controls.Add(this.blg);
            this.Controls.Add(this.cks);
            this.Controls.Add(this.grs);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.mrk);
            this.Controls.Add(this.turu);
            this.Controls.Add(this.plkNo);
            this.Controls.Add(this.arcNo);
            this.Controls.Add(this.yktTipi);
            this.Controls.Add(this.tur);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "aracbilgileri";
            this.Text = "Arac Bilgileri";
            this.Load += new System.EventHandler(this.aracbilgileri_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label tur;
        private System.Windows.Forms.Label yktTipi;
        private System.Windows.Forms.TextBox arcNo;
        private System.Windows.Forms.TextBox plkNo;
        private System.Windows.Forms.TextBox turu;
        private System.Windows.Forms.TextBox mrk;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button grs;
        private System.Windows.Forms.Button cks;
        private System.Windows.Forms.Label blg;
        private System.Windows.Forms.ComboBox peron;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label bosYer;
        private System.Windows.Forms.Button iptl;
        private System.Windows.Forms.ComboBox yktTpi;
        private System.Windows.Forms.Label label6;
    }
}