﻿namespace OtoparkTakip
{
    partial class Giris
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCks = new System.Windows.Forms.Button();
            this.btnGrs = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.kulad = new System.Windows.Forms.TextBox();
            this.sifre = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnCks
            // 
            this.btnCks.Location = new System.Drawing.Point(222, 196);
            this.btnCks.Name = "btnCks";
            this.btnCks.Size = new System.Drawing.Size(75, 23);
            this.btnCks.TabIndex = 0;
            this.btnCks.Text = "Çıkış";
            this.btnCks.UseVisualStyleBackColor = true;
            this.btnCks.Click += new System.EventHandler(this.btnCks_Click);
            // 
            // btnGrs
            // 
            this.btnGrs.Location = new System.Drawing.Point(91, 196);
            this.btnGrs.Name = "btnGrs";
            this.btnGrs.Size = new System.Drawing.Size(75, 23);
            this.btnGrs.TabIndex = 1;
            this.btnGrs.Text = "Giriş";
            this.btnGrs.UseVisualStyleBackColor = true;
            this.btnGrs.Click += new System.EventHandler(this.btnGrs_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(37, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Kullanıcı Adı:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(37, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Şifre:";
            // 
            // label3
            // 
            this.label3.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(349, 31);
            this.label3.TabIndex = 4;
            this.label3.Text = "OtoPark Takip Otomasyonu";
            // 
            // kulad
            // 
            this.kulad.Location = new System.Drawing.Point(153, 84);
            this.kulad.Name = "kulad";
            this.kulad.Size = new System.Drawing.Size(100, 20);
            this.kulad.TabIndex = 5;
            // 
            // sifre
            // 
            this.sifre.Location = new System.Drawing.Point(153, 130);
            this.sifre.Name = "sifre";
            this.sifre.Size = new System.Drawing.Size(100, 20);
            this.sifre.TabIndex = 6;
            this.sifre.UseSystemPasswordChar = true;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 310);
            this.Controls.Add(this.sifre);
            this.Controls.Add(this.kulad);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGrs);
            this.Controls.Add(this.btnCks);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCks;
        private System.Windows.Forms.Button btnGrs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox kulad;
        private System.Windows.Forms.TextBox sifre;
    }
}