﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OtoparkTakip
{
    public partial class anaSayfa : Form
    {
        public anaSayfa()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void hakkındaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            hakkinda hak = new hakkinda();
            hak.MdiParent = this;
            hak.Show();


        }

        private void kullanıcıDeğiştirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KullaniciDegistir form = new KullaniciDegistir();
            form.MdiParent = this;
            form.Show();

        }

        private void çıkışToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void girişÇıkışYapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            otogiriscikis oto = new otogiriscikis();
            oto.MdiParent = this;
            oto.Show();

            

        }

        private void kullanıcıEkleSilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KullaniciEkle ekle = new KullaniciEkle ();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void kullanıcıBilgileriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kullanicibilgileri k_bil = new kullanicibilgileri();
            k_bil.MdiParent = this;
            k_bil.Show();

        }

        private void müşteriEklemeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            musteriekleme mekle = new musteriekleme();
             mekle.Show();
        }

        private void müşteriBilgileriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            musteriekleme ekle =new musteriekleme();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void aboneYapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            aboneYap ekle = new aboneYap();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void aboneBilgileriToolStripMenuItem_Click(object sender, EventArgs e)
        {
           aboneBilgileri ekle = new aboneBilgileri();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void aboneTarifeleriToolStripMenuItem_Click(object sender, EventArgs e)
        {
           tarifeler ekle = new tarifeler();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void kasaSıfırlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kasaSifirma grs = new kasaSifirma();
            grs.MdiParent = this;
            grs.Show();
        }

        private void aboneSorgulamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
           aboneSorgulama ekle = new aboneSorgulama();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void yardımToolStripMenuItem_Click(object sender, EventArgs e)
        {
            yardim ekle = new yardim();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void kasaRaporuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kRapor ekle = new kRapor();
            ekle.MdiParent = this;
            ekle.Show();

        }

        private void grişiMakbuzlarıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            grsckssorgu ekle = new grsckssorgu();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void girişToolStripMenuItem_Click(object sender, EventArgs e)
        {
            aracGiris ekle = new aracGiris();
            ekle.MdiParent = this;
            ekle.Show();
        }

        private void çıkışToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            aracCikis ekle = new aracCikis();
            ekle.MdiParent = this;
            ekle.Show();
        }

    }
}
