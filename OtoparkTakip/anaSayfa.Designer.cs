﻿namespace OtoparkTakip
{
    partial class anaSayfa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kullanıcıDeğiştirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kullanıcıBilgileriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kullanıcıEkleSilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hakkındaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.çıkışToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otoGirişToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.girişÇıkışYapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.müşteriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.müşteriEklemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.müşteriBilgileriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abonelerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboneYapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboneBilgileriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboneTarifeleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboneSorgulamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makbuzlarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makbuzKesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.girişToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.çıkışToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.grişiMakbuzlarıToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kasaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kasaSıfırlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kasaRaporuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yardımToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.otoGirişToolStripMenuItem,
            this.müşteriToolStripMenuItem,
            this.abonelerToolStripMenuItem,
            this.makbuzlarToolStripMenuItem,
            this.kasaToolStripMenuItem,
            this.yardımToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(751, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kullanıcıDeğiştirToolStripMenuItem,
            this.kullanıcıBilgileriToolStripMenuItem,
            this.kullanıcıEkleSilToolStripMenuItem,
            this.hakkındaToolStripMenuItem,
            this.çıkışToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.dosyaToolStripMenuItem.Text = "Dosya ";
            // 
            // kullanıcıDeğiştirToolStripMenuItem
            // 
            this.kullanıcıDeğiştirToolStripMenuItem.Name = "kullanıcıDeğiştirToolStripMenuItem";
            this.kullanıcıDeğiştirToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.kullanıcıDeğiştirToolStripMenuItem.Text = "Kullanıcı Değiştir";
            this.kullanıcıDeğiştirToolStripMenuItem.Click += new System.EventHandler(this.kullanıcıDeğiştirToolStripMenuItem_Click);
            // 
            // kullanıcıBilgileriToolStripMenuItem
            // 
            this.kullanıcıBilgileriToolStripMenuItem.Name = "kullanıcıBilgileriToolStripMenuItem";
            this.kullanıcıBilgileriToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.kullanıcıBilgileriToolStripMenuItem.Text = "Kullanıcı Bilgileri";
            this.kullanıcıBilgileriToolStripMenuItem.Click += new System.EventHandler(this.kullanıcıBilgileriToolStripMenuItem_Click);
            // 
            // kullanıcıEkleSilToolStripMenuItem
            // 
            this.kullanıcıEkleSilToolStripMenuItem.Name = "kullanıcıEkleSilToolStripMenuItem";
            this.kullanıcıEkleSilToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.kullanıcıEkleSilToolStripMenuItem.Text = "Kullanıcı Ekle/Sil";
            this.kullanıcıEkleSilToolStripMenuItem.Click += new System.EventHandler(this.kullanıcıEkleSilToolStripMenuItem_Click);
            // 
            // hakkındaToolStripMenuItem
            // 
            this.hakkındaToolStripMenuItem.Name = "hakkındaToolStripMenuItem";
            this.hakkındaToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.hakkındaToolStripMenuItem.Text = "Hakkında";
            this.hakkındaToolStripMenuItem.Click += new System.EventHandler(this.hakkındaToolStripMenuItem_Click);
            // 
            // çıkışToolStripMenuItem
            // 
            this.çıkışToolStripMenuItem.Name = "çıkışToolStripMenuItem";
            this.çıkışToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.çıkışToolStripMenuItem.Text = "Çıkış";
            this.çıkışToolStripMenuItem.Click += new System.EventHandler(this.çıkışToolStripMenuItem_Click);
            // 
            // otoGirişToolStripMenuItem
            // 
            this.otoGirişToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.girişÇıkışYapToolStripMenuItem});
            this.otoGirişToolStripMenuItem.Name = "otoGirişToolStripMenuItem";
            this.otoGirişToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.otoGirişToolStripMenuItem.Text = "Oto Park";
            // 
            // girişÇıkışYapToolStripMenuItem
            // 
            this.girişÇıkışYapToolStripMenuItem.Name = "girişÇıkışYapToolStripMenuItem";
            this.girişÇıkışYapToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.girişÇıkışYapToolStripMenuItem.Text = "Giriş -Çıkış Yap";
            this.girişÇıkışYapToolStripMenuItem.Click += new System.EventHandler(this.girişÇıkışYapToolStripMenuItem_Click);
            // 
            // müşteriToolStripMenuItem
            // 
            this.müşteriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.müşteriEklemeToolStripMenuItem,
            this.müşteriBilgileriToolStripMenuItem});
            this.müşteriToolStripMenuItem.Name = "müşteriToolStripMenuItem";
            this.müşteriToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.müşteriToolStripMenuItem.Text = "Müşteri";
            // 
            // müşteriEklemeToolStripMenuItem
            // 
            this.müşteriEklemeToolStripMenuItem.Name = "müşteriEklemeToolStripMenuItem";
            this.müşteriEklemeToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.müşteriEklemeToolStripMenuItem.Text = "Müşteri Ekleme";
            this.müşteriEklemeToolStripMenuItem.Click += new System.EventHandler(this.müşteriEklemeToolStripMenuItem_Click);
            // 
            // müşteriBilgileriToolStripMenuItem
            // 
            this.müşteriBilgileriToolStripMenuItem.Name = "müşteriBilgileriToolStripMenuItem";
            this.müşteriBilgileriToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.müşteriBilgileriToolStripMenuItem.Text = "Müşteri Bilgileri ";
            this.müşteriBilgileriToolStripMenuItem.Click += new System.EventHandler(this.müşteriBilgileriToolStripMenuItem_Click);
            // 
            // abonelerToolStripMenuItem
            // 
            this.abonelerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboneYapToolStripMenuItem,
            this.aboneBilgileriToolStripMenuItem,
            this.aboneTarifeleriToolStripMenuItem,
            this.aboneSorgulamaToolStripMenuItem});
            this.abonelerToolStripMenuItem.Name = "abonelerToolStripMenuItem";
            this.abonelerToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.abonelerToolStripMenuItem.Text = "Aboneler";
            // 
            // aboneYapToolStripMenuItem
            // 
            this.aboneYapToolStripMenuItem.Name = "aboneYapToolStripMenuItem";
            this.aboneYapToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.aboneYapToolStripMenuItem.Text = "Abone Yap";
            this.aboneYapToolStripMenuItem.Click += new System.EventHandler(this.aboneYapToolStripMenuItem_Click);
            // 
            // aboneBilgileriToolStripMenuItem
            // 
            this.aboneBilgileriToolStripMenuItem.Name = "aboneBilgileriToolStripMenuItem";
            this.aboneBilgileriToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.aboneBilgileriToolStripMenuItem.Text = "Abone Bilgileri";
            this.aboneBilgileriToolStripMenuItem.Click += new System.EventHandler(this.aboneBilgileriToolStripMenuItem_Click);
            // 
            // aboneTarifeleriToolStripMenuItem
            // 
            this.aboneTarifeleriToolStripMenuItem.Name = "aboneTarifeleriToolStripMenuItem";
            this.aboneTarifeleriToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.aboneTarifeleriToolStripMenuItem.Text = "Tarifeler";
            this.aboneTarifeleriToolStripMenuItem.Click += new System.EventHandler(this.aboneTarifeleriToolStripMenuItem_Click);
            // 
            // aboneSorgulamaToolStripMenuItem
            // 
            this.aboneSorgulamaToolStripMenuItem.Name = "aboneSorgulamaToolStripMenuItem";
            this.aboneSorgulamaToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.aboneSorgulamaToolStripMenuItem.Text = "Abone Sorgulama";
            this.aboneSorgulamaToolStripMenuItem.Click += new System.EventHandler(this.aboneSorgulamaToolStripMenuItem_Click);
            // 
            // makbuzlarToolStripMenuItem
            // 
            this.makbuzlarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.makbuzKesToolStripMenuItem,
            this.grişiMakbuzlarıToolStripMenuItem});
            this.makbuzlarToolStripMenuItem.Name = "makbuzlarToolStripMenuItem";
            this.makbuzlarToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.makbuzlarToolStripMenuItem.Text = "Makbuzlar";
            // 
            // makbuzKesToolStripMenuItem
            // 
            this.makbuzKesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.girişToolStripMenuItem,
            this.çıkışToolStripMenuItem1});
            this.makbuzKesToolStripMenuItem.Name = "makbuzKesToolStripMenuItem";
            this.makbuzKesToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.makbuzKesToolStripMenuItem.Text = "Makbuz Kes";
            // 
            // girişToolStripMenuItem
            // 
            this.girişToolStripMenuItem.Name = "girişToolStripMenuItem";
            this.girişToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.girişToolStripMenuItem.Text = "Giriş";
            this.girişToolStripMenuItem.Click += new System.EventHandler(this.girişToolStripMenuItem_Click);
            // 
            // çıkışToolStripMenuItem1
            // 
            this.çıkışToolStripMenuItem1.Name = "çıkışToolStripMenuItem1";
            this.çıkışToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.çıkışToolStripMenuItem1.Text = "Çıkış";
            this.çıkışToolStripMenuItem1.Click += new System.EventHandler(this.çıkışToolStripMenuItem1_Click);
            // 
            // grişiMakbuzlarıToolStripMenuItem
            // 
            this.grişiMakbuzlarıToolStripMenuItem.Name = "grişiMakbuzlarıToolStripMenuItem";
            this.grişiMakbuzlarıToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.grişiMakbuzlarıToolStripMenuItem.Text = "Makbuz Sorgulama";
            this.grişiMakbuzlarıToolStripMenuItem.Click += new System.EventHandler(this.grişiMakbuzlarıToolStripMenuItem_Click);
            // 
            // kasaToolStripMenuItem
            // 
            this.kasaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kasaSıfırlaToolStripMenuItem,
            this.kasaRaporuToolStripMenuItem});
            this.kasaToolStripMenuItem.Name = "kasaToolStripMenuItem";
            this.kasaToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.kasaToolStripMenuItem.Text = "Kasa";
            // 
            // kasaSıfırlaToolStripMenuItem
            // 
            this.kasaSıfırlaToolStripMenuItem.Name = "kasaSıfırlaToolStripMenuItem";
            this.kasaSıfırlaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.kasaSıfırlaToolStripMenuItem.Text = "Kasa Sıfırla";
            this.kasaSıfırlaToolStripMenuItem.Click += new System.EventHandler(this.kasaSıfırlaToolStripMenuItem_Click);
            // 
            // kasaRaporuToolStripMenuItem
            // 
            this.kasaRaporuToolStripMenuItem.Name = "kasaRaporuToolStripMenuItem";
            this.kasaRaporuToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.kasaRaporuToolStripMenuItem.Text = "Kasa Raporu";
            this.kasaRaporuToolStripMenuItem.Click += new System.EventHandler(this.kasaRaporuToolStripMenuItem_Click);
            // 
            // yardımToolStripMenuItem
            // 
            this.yardımToolStripMenuItem.Name = "yardımToolStripMenuItem";
            this.yardımToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.yardımToolStripMenuItem.Text = "Yardım";
            this.yardımToolStripMenuItem.Click += new System.EventHandler(this.yardımToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(487, 373);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 29);
            this.label1.TabIndex = 1;
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // anaSayfa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 448);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "anaSayfa";
            this.Text = "Otopark Takip Sistemi";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hakkındaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem çıkışToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem müşteriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abonelerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makbuzlarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kasaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yardımToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem müşteriEklemeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem müşteriBilgileriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboneYapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboneBilgileriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboneTarifeleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboneSorgulamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makbuzKesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem girişToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem çıkışToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem grişiMakbuzlarıToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kasaSıfırlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kasaRaporuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kullanıcıDeğiştirToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem otoGirişToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem girişÇıkışYapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kullanıcıBilgileriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kullanıcıEkleSilToolStripMenuItem;
    }
}

