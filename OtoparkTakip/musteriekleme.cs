﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.OleDb;
using System.Data.Sql;

namespace OtoparkTakip
{
    public partial class musteriekleme : Form
    {
      
        public musteriekleme()
        {
            InitializeComponent();
            

        }
        

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int a;
            if (comboBox2.Text == "LPG")
                a = 1;
            else if (comboBox2.Text == "Dizel")
                a = 3;
            else if (comboBox2.Text == "Benzin")
                a = 2;
            else
                a = 4;

            int b;
            if (comboBox1.Text == "Van")
                b = 1;
            else if (comboBox1.Text == "Camlı Van")
                b = 2;
            else if (comboBox1.Text == "Sedan")
                b = 3;
            else if (comboBox1.Text == "Station Vagon")
                b = 4;
            else if (comboBox1.Text == "Hatchback")
                b = 5;
            else if (comboBox1.Text == "Kamyon")
                b = 6;
            else if (comboBox1.Text == "Otobus")
                b = 7;

            else
                b = 8;



            if (textBox1.Text == "" & textBox2.Text == "" & textBox3.Text == "" & textBox4.Text == "" & comboBox2.Text == "" & comboBox1.Text == "")

                MessageBox.Show("Lütfen bütün alanları doldurunuz. !");


            else
                try
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Baglanti"].ConnectionString);           

                    conn.Open();
                    SqlCommand kmt = new SqlCommand();
                    kmt.CommandText = "insert into musteri  (TC,Ad,Soyad,Plaka,Kasatipi,Yakittipi) values ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + b + "','" + a + "')";
                    kmt.Connection = conn;
                    kmt.ExecuteNonQuery();
                  
                    dataGridView1.Refresh();
                    DataTable tablo = new DataTable();
                    string sqlcumlesi = "Select * from musteri";

                    SqlDataAdapter dt = new SqlDataAdapter(sqlcumlesi, conn);
                    dt.Fill(tablo);
                    dataGridView1.DataSource = tablo;
                    conn.Close();
                    MessageBox.Show("Kayıt yapıldı!");
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    comboBox1.Text = "";
                    comboBox2.Text = "";
                    

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Hata" + ex);

                }
         
        }

        private void button3_Click(object sender, EventArgs e)
        { 
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Baglanti"].ConnectionString);           

                conn.Open();
                SqlCommand kmt = new SqlCommand();
                kmt.CommandText = "delete from musteri  where TC ='" + textBox1.Text + "'";
                kmt.Connection = conn;
                kmt.ExecuteNonQuery();
                dataGridView1.Refresh();
                DataTable tablo = new DataTable();
                string sqlcumlesi = "Select * from musteri";

                SqlDataAdapter dt = new SqlDataAdapter(sqlcumlesi, conn);
                dt.Fill(tablo);
                dataGridView1.DataSource = tablo;
                conn.Close();
                MessageBox.Show("Kayıt Silindi!");
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox2.Enabled = true;
                textBox3.Enabled = true;
                textBox4.Enabled = true;
                comboBox1.Enabled = true;
                comboBox2.Enabled = true;


            }
            catch (Exception ex)
            {
                MessageBox.Show("Hata" + ex);

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            textBox4.Enabled = true;
           
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;

            button4.Visible = false;
            button6.Visible = true;
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;




             
         
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                try
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Baglanti"].ConnectionString);
                   
                    conn.Open();
                    String sorgu = "select * from musteri where TC='" + textBox1.Text + "'";
                    SqlCommand cmd = new SqlCommand(sorgu, conn);
                    SqlDataReader cikti = cmd.ExecuteReader();
                    if (cikti.Read())
                    {

                        textBox2.Enabled = false;
                        textBox3.Enabled = false;
                        textBox4.Enabled = false;
                        comboBox1.Enabled = false;
                        comboBox2.Enabled = false;


                        textBox1.Text = cikti["TC"].ToString();
                        textBox2.Text = cikti["Ad"].ToString();
                        textBox3.Text = cikti["Soyad"].ToString();
                        textBox4.Text = cikti["Plaka"].ToString();


                        button3.Enabled = true;
                        button4.Enabled = true;





                    }
                    else
                    { MessageBox.Show("Kayıt Bulunamadı..."); }
                    conn.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Hata" + ex);
                }
            }
            else
            {
                MessageBox.Show("tc giriniz");

            }
        }

        private void musteriekleme_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Baglanti"].ConnectionString);
            conn.Open();
            DataTable tablo = new DataTable();
            string sqlcumlesi ="Select * from musteri";

            SqlDataAdapter dt = new SqlDataAdapter(sqlcumlesi, conn);
            dt.Fill(tablo);
            dataGridView1.DataSource = tablo;
        

            

            

            conn.Close();


            button3.Enabled = false;
            button4.Enabled = false;
            button4.Visible = true;
            button6.Visible = false;


        }

        private void button6_Click(object sender, EventArgs e)
        {
            int a;
            if (comboBox2.Text == "LPG")
                a = 1;
            else if (comboBox2.Text == "Dizel")
                a = 3;
            else if (comboBox2.Text == "Benzin")
                a = 2;
            else
                a = 4;

            int b;
            if (comboBox1.Text == "Van")
                b = 1;
            else if (comboBox1.Text == "Camlı Van")
                b = 2;
            else if (comboBox1.Text == "Sedan")
                b = 3;
            else if (comboBox1.Text == "Station Vagon")
                b = 4;
            else if (comboBox1.Text == "Hatchback")
                b = 5;
            else if (comboBox1.Text == "Kamyon")
                b = 6;
            else if (comboBox1.Text == "Otobus")
                b = 7;

            else
                b = 8;
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Baglanti"].ConnectionString);           

                conn.Open();
                SqlCommand kmt = new SqlCommand();
                kmt.CommandText = "update musteri   set TC ='" + textBox1.Text + "',Ad='" + textBox2.Text + "',Soyad='" + textBox3.Text + "',Plaka='" + textBox4.Text + "',Kasatipi='" + b + "',Yakittipi='" + a + "'";
                kmt.Connection = conn;
                kmt.ExecuteNonQuery();
                dataGridView1.Refresh();
                DataTable tablo = new DataTable();
                string sqlcumlesi = "Select * from musteri";

                SqlDataAdapter dt = new SqlDataAdapter(sqlcumlesi, conn);
                dt.Fill(tablo);
                dataGridView1.DataSource = tablo;
                conn.Close();
                MessageBox.Show("Güncelleme Yapıldı");
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
                button4.Visible = true;


            }
            catch (Exception ex)
            {
                MessageBox.Show("Hata" + ex);
            }
            button6.Visible = false;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
